/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author Joaquin
 */
public class prueba {

    /**
     * Metodo que crea ficheros de prueba de todos los tipos especificados
     *
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static void crearFicherosPrueba() throws IOException, ClassNotFoundException, ParserConfigurationException, TransformerException {
        ArticuloDAO daoArt = new ArticuloDAO();
        TiendaDAO daoTienda = new TiendaDAO();
        FicheroXML xml = new FicheroXML();
        daoArt.getListaArticulos().removeAll(daoArt.getListaArticulos());
        daoTienda.getListaTiendas().removeAll(daoTienda.getListaTiendas());
        for (int i = 0; i < 10; i++) {
            String nif = "NIF:" + i;
            int id = i;
            String nombre = "Nombre:" + i;
            int telefono = (i + 3) * 25663;
            String direccion = "Direccion:" + i;
            for (int j = 0; j < 3; j++) {
                int idArt = j;
                String nombreArt = "NombreArt:" + j;
                String descripcionArt = "DescripcionArt:" + j;
                double precioArt = Math.random() * 100;

                daoArt.getListaArticulos().add(new Articulo(idArt, id, nombreArt, descripcionArt, precioArt));
                //System.out.println(idArt + "  " + id + "  " + nombreArt + "  " + descripcionArt + "  " + precioArt);
            }

            daoTienda.getListaTiendas().add(new Tienda(id, nif, nombre, telefono, direccion));
        }
        File artText = new File("articulosText.txt");
        File tienText = new File("TiendasText.txt");
        File tienObj = new File("TiendasObj.dat");
        File artObj = new File("ArticulosObj.dat");
        File artBin = new File("ArticulosBin.dat");
        File tienBin = new File("TiendasBin.dat");
        File XML = new File("TiendasyArticulos.xml");
        daoArt.guardaFicheroObj(artObj);
        daoTienda.guardaFicheroObj(tienObj);
        daoArt.guardarFicheroBinario(artBin);
        daoTienda.guardarFicheroBinario(tienBin);
        daoArt.guardarFicheroTexto(artText);
        daoTienda.guardarFicheroTexto(tienText);
        xml.escribirFichero(XML);
        System.out.println("Se han creado todos los ficheros con los valores predeterminados");

        // daoArt.muestraArrayPorPantalla();
    }
}
