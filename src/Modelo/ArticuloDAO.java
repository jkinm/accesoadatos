/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author Joaquin
 */
public class ArticuloDAO {

    private static ArrayList<Articulo> listaArticulos = new ArrayList<Articulo>();

    /**
     * Metodo que añade un Articulo al array list
     *
     * @param a
     */
    public void anadirArticulo(Articulo a) {
        listaArticulos.add(a);
    }

    /**
     *
     * @return
     */
    public ArrayList<Articulo> getListaArticulos() {
        return listaArticulos;
    }

    /**
     *
     * @param listaArticulos
     */
    public void setListaArticulos(ArrayList<Articulo> listaArticulos) {
        ArticuloDAO.listaArticulos = listaArticulos;
    }

    /**
     * Metodo de prueba que imprime el arraylist
     */
    public void muestraArrayPorPantalla() {
        for (int i = 0; i < listaArticulos.size(); i++) {
            String cad = listaArticulos.get(i).getId() + "," + listaArticulos.get(i).getTiendaId() + "," + listaArticulos.get(i).getNombre().toString() + ","
                    + listaArticulos.get(i).getDescripcion().toString() + "," + listaArticulos.get(i).getPrecio();
            System.out.println(cad);
        }
    }

    /**
     * Metodo que carga el fichero de texto
     *
     * @param f
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void cargaFicheroTexto(File f) throws FileNotFoundException, IOException {//Carga los datos de 

        if (!f.exists()) {
            throw new FileNotFoundException("El fichero no existe");
        }

        BufferedReader br = new BufferedReader(new FileReader(f));
        listaArticulos.removeAll(listaArticulos);
        String cad = br.readLine();
        while (cad != null) {
            String[] datosArticulo = cad.split(",");
            Articulo a = new Articulo(Integer.parseInt(datosArticulo[0]), Integer.parseInt(datosArticulo[1]), datosArticulo[2], datosArticulo[3], Double.parseDouble(datosArticulo[4]));
            listaArticulos.add(a);
            cad = br.readLine();
        }

        br.close();

    }

    /**
     * Metodo que guarda el fichero de texto
     *
     * @param f
     * @throws IOException
     */
    public void guardarFicheroTexto(File f) throws IOException {
        if (!f.exists()) {
            f.createNewFile();
        }

        PrintWriter pw = new PrintWriter(new FileWriter(f));

        for (Articulo a : listaArticulos) {

            String cadena = a.getId() + "," + a.getTiendaId() + "," + a.getNombre() + "," + a.getDescripcion() + "," + a.getPrecio();
            pw.println(cadena);

        }

        pw.close();

    }

    /**
     * Metodo que guarda el fichero binario
     *
     * @param f
     * @throws IOException
     */
    public void guardarFicheroBinario(File f) throws IOException {
        if (!f.exists()) {
            f.createNewFile();
        }

        DataOutputStream dos = new DataOutputStream(new FileOutputStream(f));

        for (Articulo a : listaArticulos) {
            //Orden en que escribo: nombre, apellidos,direccion,edad
            dos.writeInt(a.getId());
            dos.writeInt(a.getTiendaId());
            dos.writeUTF(a.getNombre());
            dos.writeUTF(a.getDescripcion());
            dos.writeDouble(a.getPrecio());
        }

        dos.close();

    }

    /**
     * Metodo que carga el fichero binario
     *
     * @param f
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void CargarFicheroBinario(File f) throws FileNotFoundException, IOException {

        if (!f.exists()) {
            throw new FileNotFoundException("Fichero no encontrado");
        }

        DataInputStream dis = new DataInputStream(new FileInputStream(f));
        listaArticulos.removeAll(listaArticulos);
        boolean salir = false;
        do {
            try {
                int id = dis.readInt();
                int tiendaId = dis.readInt();
                String nombre = dis.readUTF();
                String descripcion = dis.readUTF();
                double precio = dis.readDouble();

                Articulo a = new Articulo(id, tiendaId, nombre, descripcion, precio);
                anadirArticulo(a);

            } catch (EOFException e) {
                salir = true;
            }

        } while (!salir);
    }

    /**
     * Metodo que guarda el fichero de objetos
     *
     * @param f
     * @throws IOException
     */
    public void guardaFicheroObj(File f) throws IOException {
        if (!f.exists()) {
            f.createNewFile();
        }
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
        oos.writeObject(listaArticulos);
        oos.close();

    }

    /**
     * Metodo que carga el fichero de objetos
     *
     * @param f
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void cargaFicheroObj(File f) throws IOException, ClassNotFoundException {
        if (!f.exists()) {
            throw new FileNotFoundException("Fichero no encontrado");
        }
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
        listaArticulos.removeAll(listaArticulos);
        listaArticulos = (ArrayList<Articulo>) ois.readObject();
    }

}
