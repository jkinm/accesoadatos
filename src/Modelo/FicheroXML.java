/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author Joaquin
 */
public class FicheroXML {

    /**
     * Metodo que escribe el fichero XML
     *
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws IOException
     */
    public void escribirFichero(File fichero) throws ParserConfigurationException, TransformerConfigurationException, TransformerException, IOException {
        ArticuloDAO daoArt = new ArticuloDAO();
        TiendaDAO daoTienda = new TiendaDAO();
        if (!fichero.exists()) {
            fichero.createNewFile();
        }
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation implementation = builder.getDOMImplementation();
        Document document = implementation.createDocument(null, "TIENDAS_Y_ARTICULOS", null);
        document.setXmlVersion("1.0");

        //Main node
        Element raiz = document.getDocumentElement();
        //Defino Tiendas
        Element tiendasNode = document.createElement("TIENDAS");
        for (Tienda t : daoTienda.getListaTiendas()) {
            //defino tienda
            Element tiendaNode = document.createElement("TIENDA");
            //defino los atributos de tienda y les doy valor
            Element NIFNode = document.createElement("NIF");
            Text NIFNodeValue = document.createTextNode(t.getNif());
            NIFNode.appendChild(NIFNodeValue);

            Element idNode = document.createElement("ID");
            Text idNodeValue = document.createTextNode(String.valueOf(t.getId()));
            idNode.appendChild(idNodeValue);

            Element nombreNode = document.createElement("NOMBRE");
            Text nombreNodeValue = document.createTextNode(t.getNombre());
            nombreNode.appendChild(nombreNodeValue);

            Element tlfNode = document.createElement("TELEFONO");
            Text tlfNodeValue = document.createTextNode(String.valueOf(t.getTelefono()));
            tlfNode.appendChild(tlfNodeValue);

            Element direccionNode = document.createElement("DIRECCION");
            Text direccionNodeValue = document.createTextNode(t.getDireccion());
            direccionNode.appendChild(direccionNodeValue);

            //defino articulos de tienda
            Element articulosTiendaNode = document.createElement("ARTICULOS_TIENDA");
            for (Articulo a : daoArt.getListaArticulos()) {
                if (a.getTiendaId() == t.getId()) {
                    Element articuloNode = document.createElement("ARTICULO");

                    //atributos
                    Element idArticuloNode = document.createElement("ID_ARTICULO");
                    Text idArticuloNodeValue = document.createTextNode(String.valueOf(a.getId()));
                    idArticuloNode.appendChild(idArticuloNodeValue);

                    Element nombreArticuloNode = document.createElement("NOMBRE_ARTICULO");
                    Text nombreArticuloNodeValue = document.createTextNode(a.getNombre());
                    nombreArticuloNode.appendChild(nombreArticuloNodeValue);

                    Element descripcionArticuloNode = document.createElement("DESCRIPCION");
                    Text descripcionArticuloNodeValue = document.createTextNode(a.getDescripcion());
                    descripcionArticuloNode.appendChild(descripcionArticuloNodeValue);

                    Element precioNode = document.createElement("PRECIO");
                    Text precioNodeValue = document.createTextNode(String.valueOf(a.getPrecio()));
                    precioNode.appendChild(precioNodeValue);

                    articuloNode.appendChild(idArticuloNode);
                    articuloNode.appendChild(nombreArticuloNode);
                    articuloNode.appendChild(descripcionArticuloNode);
                    articuloNode.appendChild(precioNode);

                    articulosTiendaNode.appendChild(articuloNode);
                }
            }
            //Introduzco los hijos de Tienda
            tiendaNode.appendChild(NIFNode);
            tiendaNode.appendChild(idNode);
            tiendaNode.appendChild(nombreNode);
            tiendaNode.appendChild(tlfNode);
            tiendaNode.appendChild(direccionNode);
            tiendaNode.appendChild(articulosTiendaNode);
            tiendasNode.appendChild(tiendaNode);

        }
        raiz.appendChild(tiendasNode);
        //generamos el xml
        Source source = new DOMSource(document);
        //indicamos donde se almacena
        Result result = new StreamResult(fichero);
        Transformer transfomer = TransformerFactory.newInstance().newTransformer();
        transfomer.transform(source, result);

    }

    /**
     * Metodo que carga el fichero XML
     *
     * @param fichero
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void leerFichero(File fichero) throws ParserConfigurationException, SAXException, IOException {
        ArticuloDAO daoArt = new ArticuloDAO();
        TiendaDAO daoTienda = new TiendaDAO();
        if (!fichero.exists()) {
            throw new FileNotFoundException("El fichero no existe");
        }
//Arraylists para cargar los objetos  
        ArrayList<Articulo> arrayArticulos = new ArrayList<>();
        ArrayList<Tienda> arrayTiendas = new ArrayList<>();
        //cargamos el xml
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(fichero);
        document.getDocumentElement().normalize();
        //creamos la lista de tiendas
        NodeList listaNodos = document.getDocumentElement().getElementsByTagName("TIENDA");
        Node nodo;
        for (int i = 0; i < listaNodos.getLength(); i++) {
            nodo = listaNodos.item(i);
            NodeList listaNodosTienda = nodo.getChildNodes();
            Node atributos;
            String nif = null, nombre = null, direccion = null;
            int id = 0, telefono = 0;

            for (int j = 0; j < listaNodosTienda.getLength(); j++) {
                atributos = listaNodosTienda.item(j);
                if (atributos.getNodeName().equals("NIF")) {
                    nif = atributos.getTextContent();
                }
                if (atributos.getNodeName().equals("ID")) {
                    id = Integer.parseInt(atributos.getTextContent());
                }
                if (atributos.getNodeName().equals("NOMBRE")) {
                    nombre = atributos.getTextContent();
                }
                if (atributos.getNodeName().equals("TELEFONO")) {
                    telefono = Integer.parseInt(atributos.getTextContent());
                }
                if (atributos.getNodeName().equals("DIRECCION")) {
                    direccion = atributos.getTextContent();
                }
                if (atributos.getNodeName().equals("ARTICULOS_TIENDA")) {
                    NodeList ListaNodosArticulos = atributos.getChildNodes();
                    Node nodoArticulos;
                    for (int k = 0; k < ListaNodosArticulos.getLength(); k++) {
                        nodoArticulos = ListaNodosArticulos.item(k);
                        if (nodoArticulos.getNodeName().equals("ARTICULO")) {
                            NodeList listaNodoArt = nodoArticulos.getChildNodes();
                            Node nodoArt;
                            String nombreArt = null, descripcionArt = null;
                            int idArt = 0;
                            double precio = 0;
                            for (int l = 0; l < listaNodoArt.getLength(); l++) {
                                nodoArt = listaNodoArt.item(l);
                                NodeList atributosArt = nodoArt.getChildNodes();
                                Node atribArt;
                                if (nodoArt.getNodeName().equals("ID_ARTICULO")) {
                                    idArt = Integer.parseInt(nodoArt.getTextContent());
                                }
                                if (nodoArt.getNodeName().equals("NOMBRE_ARTICULO")) {
                                    nombreArt = nodoArt.getTextContent();
                                }
                                if (nodoArt.getNodeName().equals("DESCRIPCION")) {
                                    descripcionArt = nodoArt.getTextContent();
                                }
                                if (nodoArt.getNodeName().equals("PRECIO")) {
                                    precio = Double.parseDouble(nodoArt.getTextContent());
                                }
                            }
                            arrayArticulos.add(new Articulo(idArt, id, nombre, descripcionArt, precio));

                        }
                    }
                }

            }
            arrayTiendas.add(new Tienda(id, nif, nombre, telefono, direccion));

        }
        daoTienda.setListaTiendas(arrayTiendas);
        daoArt.setListaArticulos(arrayArticulos);

    }

}
