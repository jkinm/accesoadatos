/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author Joaquin
 */
public class TiendaDAO {

    private static ArrayList<Tienda> listaTiendas = new ArrayList<Tienda>();

    /**
     * Añade una tienda al arraylist
     *
     * @param t
     */
    public void anadirTienda(Tienda t) {
        listaTiendas.add(t);
    }

    /**
     *
     * @return
     */
    public ArrayList<Tienda> getListaTiendas() {
        return listaTiendas;
    }

    /**
     *
     * @param listaTiendas
     */
    public void setListaTiendas(ArrayList<Tienda> listaTiendas) {
        TiendaDAO.listaTiendas = listaTiendas;
    }

    /**
     * Metodo de prueba que muestra el arraylist por pantalla
     */
    public void muestraArrayPorPantalla() {
        for (int i = 0; i < listaTiendas.size(); i++) {
            String cad = listaTiendas.get(i).getId() + "," + listaTiendas.get(i).getNif() + "," + listaTiendas.get(i).getNombre()
                    + "," + listaTiendas.get(i).getTelefono() + "," + listaTiendas.get(i).getDireccion();
            System.out.println(cad);
        }
    }

    /**
     * Metodo que carga el fichero de texto
     *
     * @param f
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void cargaFicheroTexto(File f) throws FileNotFoundException, IOException {//Carga los datos de 

        if (!f.exists()) {
            throw new FileNotFoundException("El fichero no existe");
        }

        BufferedReader br = new BufferedReader(new FileReader(f));
        listaTiendas.removeAll(listaTiendas);
        String cad = br.readLine();
        while (cad != null) {
            String[] datosTiendas = cad.split(",");
            Tienda t = new Tienda(Integer.parseInt(datosTiendas[0]), datosTiendas[1], datosTiendas[2],
                    Integer.parseInt(datosTiendas[3]), datosTiendas[4]);
            listaTiendas.add(t);
            cad = br.readLine();
        }

        br.close();

    }

    /**
     * Metodo que guarda el fichero de texto
     *
     * @param f
     * @throws IOException
     */
    public void guardarFicheroTexto(File f) throws IOException {
        if (!f.exists()) {
            f.createNewFile();
        }

        PrintWriter pw = new PrintWriter(new FileWriter(f));

        for (Tienda t : listaTiendas) {

            String cadena = t.getId() + "," + t.getNif() + "," + t.getNombre() + "," + t.getTelefono() + "," + t.getDireccion();
            pw.println(cadena);

        }

        pw.close();

    }

    /**
     * Metodo que guarda el fichero binario
     *
     * @param f
     * @throws IOException
     */
    public void guardarFicheroBinario(File f) throws IOException {
        if (!f.exists()) {
            f.createNewFile();
        }

        DataOutputStream dos = new DataOutputStream(new FileOutputStream(f));

        for (Tienda t : listaTiendas) {
            //Orden en que escribo: nombre, apellidos,direccion,edad
            dos.writeInt(t.getId());
            dos.writeUTF(t.getNif());
            dos.writeUTF(t.getNombre());
            dos.writeInt(t.getTelefono());
            dos.writeUTF(t.getDireccion());
        }

        dos.close();

    }

    /**
     * Metodo que carga el fichero binario
     *
     * @param f
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void CargarFicheroBinario(File f) throws FileNotFoundException, IOException {

        if (!f.exists()) {
            throw new FileNotFoundException("Fichero no encontrado");
        }

        DataInputStream dis = new DataInputStream(new FileInputStream(f));
        listaTiendas.removeAll(listaTiendas);
        boolean salir = false;
        do {
            try {
                int id = dis.readInt();
                String nif = dis.readUTF();
                String nombre = dis.readUTF();
                int telefono = dis.readInt();
                String direccion = dis.readUTF();

                Tienda t = new Tienda(id, nif, nombre, telefono, direccion);
                anadirTienda(t);

            } catch (EOFException e) {
                salir = true;
            }

        } while (!salir);
    }

    /**
     * Metodo que guarda el fichero de objetos
     *
     * @param f
     * @throws IOException
     */
    public void guardaFicheroObj(File f) throws IOException {
        if (!f.exists()) {
            f.createNewFile();
        }
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
        oos.writeObject(listaTiendas);
        oos.close();

    }

    /**
     * Metodo que carga el fichero de objetos
     *
     * @param f
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void cargaFicheroObj(File f) throws IOException, ClassNotFoundException {
        if (!f.exists()) {
            throw new FileNotFoundException("Fichero no encontrado");
        }
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
        listaTiendas.removeAll(listaTiendas);
        listaTiendas = (ArrayList<Tienda>) ois.readObject();
    }

}
